from peewee import *


proxy = Proxy()

class Customer(Model):

    first_nm = CharField(max_length=100) 
    last_nm = CharField(max_length=100)
    cust_id = AutoField(primary_key=True)
    class Meta:
        database = proxy
        table_name = "customers"


class Good(Model):
    
    good_id = AutoField(primary_key=True) 
    vendor = CharField(max_length=100) 
    name = CharField(max_length=100)
    description = CharField(max_length=300)
    class Meta:
        database = proxy
        table_name =  "goods"


class Order(Model):

    order_id = AutoField(primary_key=True)
    cust = ForeignKeyField(Customer, on_delete="CASCADE") 
    order_dttm = DateTimeField()
    status = CharField(max_length=20)

    class Meta:
        database = proxy
        table_name = "orders"


class OrderItems(Model):

    order_item_id = AutoField(primary_key=True)
    order = ForeignKeyField(Order, on_delete="CASCADE") 
    good = ForeignKeyField(Good, on_delete="CASCADE")
    quantity = IntegerField()

    class Meta:
        database = proxy
        table_name = "order_items" 

