import configparser
import datetime
import itertools
import peewee as pw
from datamodel import *


CONFIG_FILE_NAME = "conf.ini"
DB_SECTION = "database"


def parseConfig(fileName):
    config = configparser.ConfigParser()
    config.read(fileName)
    return config     


def getSectionConfig(sectionName):
    config = parseConfig(CONFIG_FILE_NAME)
    if sectionName in config.sections():
        return config[sectionName]
    else:
        raise ValueError("Can't find " + sectionName + " section")  


def downloadToFile(fileName):
    data = (OrderItems.select(Order.order_id, 
                              Customer.first_nm,
                              Customer.last_nm,
                              OrderItems.order_item_id,
                              Good.good_id,
                              Good.name,
                              Good.vendor,
                              OrderItems.quantity)
            .join_from(OrderItems, Order)
            .join_from(Order, Customer)
            .join_from(OrderItems, Good)
            .dicts())

    groupedData = itertools.groupby(sorted(list(data), key=lambda y: y["order_id"]), lambda x: x["order_id"])
    with open(fileName, "w") as file: 
        for key, group in groupedData:
            file.write("Order_id: " + str(key))
            group = list(group)
            file.write("\nCustomer name: " + group[0]["first_nm"] + " " + group[0]["last_nm"])
            for good in group:
                file.write("\n   ord_item_id = " + str(good["order_item_id"]))
                file.write("\n   good_id = " + str(good["good_id"]))
                file.write("\n   vendor = " + good["vendor"])
                file.write("\n   name = " + good["name"])
                file.write("\n   quantity = " + str(good["quantity"]) + "\n")
            file.write("\n\n\n")


def deleteGoodFromOrder(order, good):
    return OrderItems.delete().where(OrderItems.order==order, OrderItems.good==good).execute() 


def addGoodToOrder(order, good):
    return OrderItems.insert(order=order, good=good, quantity=0).execute()


def updateOrderItemQuantity(orderItem, newQuantity):
    return OrderItems.update(quantity=newQuantity).where(OrderItems.order_item_id==orderItem).execute()


def main():
    dbConf = getSectionConfig(DB_SECTION) 
    db = pw.PostgresqlDatabase(dbConf["DbName"], user=dbConf["User"], password=dbConf["Password"], port=dbConf["Port"])
    proxy.initialize(db)

    customers = Customer(db) 
    goods = Good(db) 
    orders = Order(db) 
    orderItems = OrderItems(db)

    tables = [customers, goods, orders, orderItems]
    for table in tables:
        table.drop_table(cascade=True)
        table.create_table()

    cust_1 = customers.insert(first_nm="first_customer_1" , last_nm="last_customer_1").execute()  
    cust_2 = customers.insert(first_nm="first_customer_2" , last_nm="last_customer_2").execute()  

    good_1 = goods.insert(vendor="vendor_1", description="description_1", name="good_1").execute()
    good_2 = goods.insert(vendor="vendor_2", description="description_2", name="good_2").execute()

    order_1 = orders.insert(order_dttm=datetime.date(2018, 1, 10), status="Ok", cust=cust_1).execute()
    order_2 = orders.insert(order_dttm=datetime.date(2018, 4, 8), status="Okk", cust=cust_2).execute()

    orderItems.insert(order=order_1, good=good_1, quantity=101).execute()
    orderItems.insert(order=order_1, good=good_2, quantity=10).execute()
    orderItems.insert(order=order_2, good=good_2, quantity=1).execute()

    # 1) delete order from order 
    deleteGoodFromOrder(order_1, good_2) 

    # 2) add order item to order
    new_item = addGoodToOrder(order_1, good_2)

    # 3) update order item quantity
    newQuantity = 95
    updateOrderItemQuantity(new_item, newQuantity)

    downloadToFile("allOrders.txt")


if __name__ == "__main__":
    main()
