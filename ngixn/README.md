## Добавляем Nginx-репозиторий:  
#
```bash
$ sudo rpm --import https://nginx.org/keys/nginx_signing.key
$ sudo rm -rf /etc/yum.repos.d/puppet5.repo
$ sudo cat > /etc/yum.repos.d/nginx.repo << 'EOF'
[nginx.org]
name=Nginx official repo (binaries)
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
enabled=1

[nginx.org-src]
name=Nginx official repo (sources)
baseurl=http://nginx.org/packages/centos/7/SRPMS/
enabled=1
EOF

$ sudo yum clean all && yum install -y nginx
```

Создаем конфиг по пути `/etc/nginx/conf.d/1-test.conf`:
#
```
server {
    server_name  test-1;
    listen       8001;
    root /var/www/test-1;
}
```  

## Разместим тестовую индекс страницу по адресу:
$ cat /var/www/test-1/index.html
Yohoo, it works

## Добавляем прав SELinux на каталоги, ианче у пользователя Nginx Не будет прав читать файлы
sudo chcon -Rt httpd_sys_content_t /var/www

## Перезагружаем nginx, чтобы применить конифгурацию
sudo nginx -s reload

## Сделаем так, чтобы запросы с этой же машины по имени `test-1` и `test-2` приходили на неё же
Отредактируем файл /etc/hosts, добавив в него имена `test-1` и `test-2`. 
Из-за того, что в nginx нет блока сервера с именем test-2, то он будет отдавать первый попавшийся серверный блок (он фактически будет блоком по умолчанию).

## Проверяем, что всё работает:
$ curl test-1
Yohoo, it works

## Вопрос - почему намш конфиг используется nginx-ом? Как/почему он его подтягивает?
Основная конфигурация содержится в файле /etc/nginx/nginx.conf. В нем как раз содержится блок http{ }, внутри которого распологаются блоки с серверами. 
Так, в нем содержится строка:
```
include /etc/nginx/conf.d/*.conf;
```
Она как раз и подтягивает наш файл test-1.conf для использования nginx-ом.

## Тестируем дальше
$ curl test-2
Yohoo, it works
### Ooouch...!

* Подправить конфиг `/etc/nginx/conf.d/default.conf` таким образом, чтобы:
  * Он был конфигом по умолчанию (отдавался на все явно не указанные server_name на 8001-м порту)
  * Минимальным (убрать из него лишние комментарии и секции)
  * На все запросы отдавался бы код `404` с телом `'No <имя сервера> server config found` (подсказка см. `return`)

`default.conf`:
#
```
server {
    listen       8001 default_server;
    # название сервера "_" является одним из некорректных доменных имён, которые никогда не пересекутся ни с одним из реальных имен.
    server_name  _;
    location / {
        # Указывается для того, чтобы показывать содержимое "No $hostname server config found" в браузере ввиде текста. 
    	default_type text/plain;
    	return 404 "No $hostname server config found";
    }
}
```
#

Убеждаемся что теперь запрос `curl test-2` не отдаёт нам первый попавшийся сайт.





