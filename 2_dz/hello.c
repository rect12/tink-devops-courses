#include <sys/types.h>
#include <unistd.h>

int main() {
    pid_t pid =  fork();
    puts("Hello world!");
    return 0;
}
