import configparser
import datetime
import itertools
from dataBase import DataBase
from create_tables import *
from datamodel import *
from tables_operations import * 

CONFIG_FILE_NAME = "conf.ini"
DB_SECTION = "database"


def parseConfig(fileName):
    config = configparser.ConfigParser()
    config.read(fileName)
    return config     


def getSectionConfig(sectionName):
    config = parseConfig(CONFIG_FILE_NAME)
    if sectionName in config.sections():
        return config[sectionName]
    else:
        raise ValueError("Can't find " + sectionName + " section")  


def downloadToFile(data, fileName):
    data = itertools.groupby(data, lambda x: x[0])
    order_id = 0
    cust_firs_nm = 1
    cust_last_nm = 2
    order_item_id = 3 
    good_id = 4 
    good_vendor = 6
    good_name = 5 
    good_quantity = 7

    with open(fileName, "w") as file:
        for key, group in data: 
            file.write("Order_id: " + str(key))
            group = list(group)
            file.write("\nCustomer name: " + group[0][cust_firs_nm] + group[0][cust_last_nm])
            for good in group:
                file.write("\n   ord_item_id = " + str(good[order_item_id]))
                file.write("\n   good_id = " + str(good[good_id]))
                file.write("\n   vendor = " + good[good_vendor])
                file.write("\n   name = " + good[good_name])
                file.write("\n   quantity = " + str(good[good_quantity]) + "\n")
            file.write("\n\n\n")



def main():
    dbConf = getSectionConfig(DB_SECTION) 
    db = DataBase(dbConf["Port"], dbConf["DbName"], dbConf["User"], dbConf["Password"])

    db.query(dropTables, None)
    db.query(createTableCustomers, None)
    db.query(createTableOrders, None) 
    db.query(createTableGoods, None)
    db.query(createTableOrderItems, None)
    
    customer1 = Customer("John", "Smith")
    customer2 = Customer("Aa", "Bb")

    good1 = Good("vendor_1", "good_1", "best good ever")
    good2 = Good("vendor_2", "good_2", "worst good ever")
    
    insertCustomer(db, customer1)
    insertCustomer(db, customer2)
    insertGood(db, good1)
    insertGood(db, good2)

    order1 = Order(customer1, datetime.datetime.now(), "ok")
    insertOrder(db, order1)
    orderItem1 = OrderItem(order1, good1, 10)
    orderItem2 = OrderItem(order1, good2, 150)
    insertOrderItem(db, orderItem1)
    insertOrderItem(db, orderItem2)

    deleteGoodFromOrder(db, good1, order1)
    orderItem3 = addGoodToOrder(db, good1, order1)
    updateOrderItemQuantity(db, orderItem3, 11)

    downloadToFile(getAllOrders(db), "allOrders.txt")


if __name__ == "__main__":
    main()
