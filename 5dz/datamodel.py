class Customer(object):

    cust_id = None

    def __init__(self, first_nm, last_nm):
        self.first_nm = first_nm
        self.last_nm = last_nm
        self.cust_id = None


class Order(object):

    order_id = None

    def __init__(self, customer, order_dttm, status):
        self.customer = customer 
        self. order_dttm = order_dttm
        self.status = status


class OrderItem(object):

    order_item_id = None

    def __init__(self, order, good, quantity):
        self.order = order
        self.good = good
        self.quantity = quantity


class Good(object):
    
    good_id = None

    def __init__(self, vendor, name, description):
        self.vendor = vendor
        self.name = name
        self.description = description
