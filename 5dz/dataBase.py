import psycopg2

class DataBase(object):

    connection = None
    cursor = None

    def __init__(self, port, dbName, user, password):
        self.connection = self._initConnection(port, dbName, user, password)

    def _initConnection(self, port, dbName, user, password):
        return psycopg2.connect(port=port, database=dbName, user=user, password=password) 

    def _closeConnection(self):
        self.connection.close()
        
    def __del__(self):
        self._closeConnection()

    def query(self, query, values):
        try:
            with self.connection.cursor() as cur:
                result = cur.execute(query, values)
            self.connection.commit()
        except Exception as ex:
            print("error while executing: " + query + ", error: " + str(ex))
            return None
        else:
            return result

    def queryWithReturning(self, query, values):
        try:
            with self.connection.cursor() as cur:
                cur.execute(query, values)
                result = cur.fetchone()[0]
            self.connection.commit()
        except Exception as ex:
            print("error while executing: " + query + ", error: " + str(ex))
            return None
        else:
            return result


    def selectQuery(self, query, values):
        try:
            with self.connection.cursor() as cur:
                cur.execute(query, values)
                result = cur.fetchall()
            self.connection.commit()
        except Exception as ex:
            print("error while executing: " + query + ", error: " + str(ex))
            return None
        else:
            return result

