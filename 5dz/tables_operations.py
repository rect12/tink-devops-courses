from dataBase import DataBase
from datamodel import *

def insertCustomer(db, customer):
    sqlStr = '''
    insert into customers(first_nm, last_nm) values (%s, %s)
    returning cust_id;
    '''

    customer.cust_id = db.queryWithReturning(sqlStr, (customer.first_nm, customer.last_nm))
    return customer.cust_id


def insertGood(db, good):
    sqlStr = '''
    insert into goods
    (vendor, name, description) values
    (%s,%s,%s)
    returning good_id;
    '''

    good.good_id = db.queryWithReturning(sqlStr, (good.vendor, good.name, good.description))
    return good.good_id
    

def insertOrder(db, order):
    sqlStr = '''
    insert into orders
    (cust_id, order_dttm, status) values
    (%s,%s,%s)
    returning order_id;
    '''
    order.order_id = db.queryWithReturning(sqlStr, (order.customer.cust_id, order.order_dttm, order.status))
    return order.order_id


def insertOrderItem(db, orderItem):
    sqlStr = '''
    insert into order_items
    (order_id, good_id, quantity) values
    (%s,%s,%s)
    returning order_item_id;
    '''

    orderItem.order_item_id =  db.queryWithReturning(sqlStr, (orderItem.order.order_id, orderItem.good.good_id, orderItem.quantity))
    return orderItem.order_item_id


def deleteGoodFromOrder(db, good, order):
    sqlStr = '''
    delete from order_items 
    where good_id= %s and order_id= %s 
    '''
    db.query(sqlStr, (good.good_id, order.order_id))


def addGoodToOrder(db, good, order):
    orderItem = OrderItem(order, good, 0)
    insertOrderItem(db, orderItem)
    return orderItem


def updateOrderItemQuantity(db, orderItem, newQuantity):
    sqlStr = '''
    update order_items
    set quantity = %s
    where order_item_id = %s
    '''
    db.query(sqlStr, (newQuantity, orderItem.order_item_id))


def getGood(db, goodId):
    sqlStr = '''
    select * from goods
    where good_id = %s
    '''
    tmp = db.selectQuery(sqlStr,(goodId))
    good = Good(tmp[1], tmp[2], tmp[3])  
    good.good_id = tmp[0]
    return good


def getAllOrders(db):
    getOrderItemsSql = '''
    select ord.order_id,
           cust.first_nm,
           cust.last_nm,
           ord_it.order_item_id,
           goods.good_id,
           goods.name,
           goods.vendor,
           ord_it.quantity
    from order_items ord_it 
    join orders ord on (ord_it.order_id = ord.order_id) 
    join customers as cust on cust.cust_id = ord.cust_id 
    join goods on goods.good_id = ord_it.good_id;
    '''
    ordItm = db.selectQuery(getOrderItemsSql , None) 
    return ordItm

    
    


