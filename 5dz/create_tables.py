dropTables =  '''
drop table if exists customers cascade;
drop table if exists orders cascade;
drop table if exists goods cascade;
drop table if exists order_items cascade;
'''

createTableCustomers = '''
create table customers (
    cust_id serial primary key,
    first_nm varchar(100),
    last_nm varchar(100)
);
'''

createTableOrders = '''
create table orders (
    order_id serial primary key,
    cust_id int,
    order_dttm timestamp,
    status varchar(20),
    foreign key(cust_id) references customers(cust_id)
);
'''

createTableGoods = '''
create table goods (
    good_id serial primary key,
    vendor varchar(100),
    name varchar(100),
    description varchar(300)
);
'''

createTableOrderItems = '''
create table order_items (
    order_item_id serial primary key, 
    order_id int,
    good_id int,
    quantity int,
    foreign key(order_id) references orders(order_id),
    foreign key(good_id) references goods(good_id)
);
'''
