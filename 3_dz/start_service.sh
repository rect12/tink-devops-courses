#!/bin/sh

service="httpd"
result=$(ps -aux | grep -v grep | grep $service | wc -l)

log_path=/root/httpd_start.log
date_time="$(date +"%y-%m-%d %T") $@"
log_message=$date_time" httpd was not running, starting..."

script_path="$(dirname $0)"
service_path=$script_path/$service

if [ $result -eq 0 ]
then
        if [ $(id -u) = 0 ]
        then
                echo $log_message >> $log_path
                $service_path &
        else
                echo "you need to be root to write log and starting $service"
        fi

fi

